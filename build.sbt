organization := "net.xymox"

name := "akka-grpc-client"

version := "0.1"

scalaVersion := "2.13.0"

enablePlugins(AkkaGrpcPlugin)

libraryDependencies += "net.xymox" %% "akka-grpc-server" % "0.1" % "protobuf"
